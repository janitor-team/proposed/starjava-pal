Source: starjava-pal
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant, debhelper-compat (= 12), dh-exec, javahelper
Build-Depends-Indep: default-jdk, default-jdk-doc
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/starjava-pal
Vcs-Git: https://salsa.debian.org/debian-astro-team/starjava-pal.git
Homepage: https://github.com/Starlink/starjava/tree/master/pal

Package: starlink-pal-java
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Description: Starlink Positional Astronomy Library (Java version)
 This library is a collection of code designed to aid in replacing the SLA
 library, implemented in pure Java.
 .
 Note that differently from the starlink-ast package, only the most important
 functions are implemented.

Package: starlink-pal-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Description: Starlink Positional Astronomy Library (Java version) - documentation
 This library is a collection of code designed to aid in replacing the SLA
 library, implemented in pure Java.
 .
 Note that differently from the starlink-ast package, only the most important
 functions are implemented.
 .
 This package contains the JavaDoc documentation of the package.
